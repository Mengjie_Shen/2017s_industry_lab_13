package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mshe666 on 18/12/2017.
 */
public class ExerciseFive {
    public void start() {

        System.out.printf("enter a number n: ");
        String inputStr = Keyboard.readInput();
        long N = Long.parseLong(inputStr);
        PrimeFactorsTask primeFactorsTask = new PrimeFactorsTask(N);
        Thread thread1 = new Thread(primeFactorsTask);

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("To abort the computation, press any key & ENTER to stop PrimeFactorsTask! ");
                String  string = Keyboard.readInput();
                thread1.interrupt();
            }
        });


        thread1.start();
        thread2.setDaemon(true);
        thread2.start();

        while (!thread1.isInterrupted()) {
            try {
                List<Long> result =  primeFactorsTask.getPrimeFactors();
                HashSet<Long> resultSet = new HashSet(result);
                for (Long l : resultSet) {
                    System.out.println(l);
                }
                break;
            }catch (IllegalStateException e) {
                e.getStackTrace();
            }
        }



//        System.out.println(thread2.getState());
//        System.exit(0);

    }

    public static void main(String[] args) {
        ExerciseFive e = new ExerciseFive();
        e.start();
    }

}
