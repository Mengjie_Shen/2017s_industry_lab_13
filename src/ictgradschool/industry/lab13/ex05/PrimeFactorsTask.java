package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mshe666 on 18/12/2017.
 */
public class PrimeFactorsTask implements Runnable {
    private List<Long> primeNumbers;
    private TaskState taskState;
    private long N;
//    private boolean interrupted;


    @Override
    public void run() {

        PrimeFactorsTask primeFactorsTask = new PrimeFactorsTask(N);
        //case one: n is an even number, it can be divided by 2
        //until the result becomes an odd number, go to case two
        while (N % 2 == 0 && !Thread.currentThread().isInterrupted()) {
//            System.out.println("This is a while loop start");
//            System.out.println(!this.interrupted);
            this.primeNumbers.add(Long.valueOf(2));
            N = N / 2;
//            System.out.println(N);
//            Thread.currentThread().isInterrupted()
//            System.out.println("This is the while loop end");
        }
//        System.out.println("in run case 1" + this.primeNumbers);

        //case two: n is an odd number, try to divide n by all odd numbers starting from 3
        //until it reaches the square root of n, go to case three
        for (long i = 3; i <= N && !Thread.currentThread().isInterrupted(); i += 2) {
            while (N % i == 0 && !Thread.currentThread().isInterrupted()) {
//                System.out.println("For loop start");
//                System.out.println(!this.interrupted);

                this.primeNumbers.add(i);
                N = N / i;
//                System.out.println("for loop end");
            }
        }
//        System.out.println("in run case 2" + this.primeNumbers);

        //case three: there's no prime number that can divide n except itself
//        if (N > 2) {
//            this.primeNumbers.add(N);
//        }
//        System.out.println("in run case 3" + this.primeNumbers);

        this.taskState = TaskState.Completed;

    }

    public PrimeFactorsTask(long N) {
        this.primeNumbers = new ArrayList<>();
        this.taskState = TaskState.Initialized;
        this.N = N;
//        this.interrupted = false;


    }
    //method can be called to return the parameter passed into the PrimeFactorsTask method
    public long n() {
        return this.N;
    }

    //method can be called to return the result List<long> of PrimeFactorTask method
    public List<Long> getPrimeFactors() throws IllegalStateException {
//        System.out.println("in getPrimeFactors " + this.getState());
//        System.out.println("in getPrimeFactors " + this.N);
//        System.out.println("in getPrimeFactors " + this.primeNumbers);
        if (this.getState().equals(TaskState.Completed)) {
//            System.out.println("in getPrimeFactors " + this.getState());
//            System.out.println("in getPrimeFactors " + this.primeNumbers);
            return this.primeNumbers;
        }else {
            throw new IllegalStateException("IllegalStateException");
        }
    }

    //method can be called to return the taskState of PrimeFactorTask method
    public TaskState getState() {
        return this.taskState;
    }

    //enum TaskState
    public enum  TaskState {
        Initialized, Completed, Aborted
    }

//    public boolean isInterrupted() {
//        return this.interrupted;
//    }

/*    public void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }*/


}
