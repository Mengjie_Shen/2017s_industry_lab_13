package ictgradschool.industry.lab13.ex03;

import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {
    private int threadCount = 1000;
    private static long start;
    private static long end;

    public void start() {
        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        long numSamples = Long.parseLong(Keyboard.readInput());
        start = 0;
        end = start + numSamples / threadCount;

        System.out.println("Estimating PI...");
        long startTime = System.currentTimeMillis();

        // Do the estimation
        double estimatedPi = estimatePI(numSamples);

        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(estimatedPi - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");

    }

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.
        ArrayList<Thread> threadList = new ArrayList<>();
        ArrayList<Double> resultList = new ArrayList<>();

        for (int i = 0; i < threadCount; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    ThreadLocalRandom tlr = ThreadLocalRandom.current();

                    long numInsideCircle = 0;
                    for (long j = start; j < end; j++) {
                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();
                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }
                    }
                    double estimatedPi = 4.0 * (double) numInsideCircle / (double) (numSamples / threadCount);
                    resultList.add(estimatedPi);
                }
            });
            t.start();
            threadList.add(t);



            start = end;
            end = start + numSamples / threadCount;
        }

        for (Thread t : threadList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        double sum = 0;
        for (Double result : resultList) {
            sum += result;
        }

        return sum / threadCount;

    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
