package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Mengjie
 * Date : 2017/12/12
 * Time : 21:36
 **/
public class ConcurrentBankingApp {
    ArrayList<Thread> threadList = new ArrayList<>();
    ArrayBlockingQueue<Transaction> transactionBlockingQueue = new ArrayBlockingQueue<Transaction>(10);
    BankAccount account = new BankAccount();
    private Lock lock = new ReentrantLock();
    private boolean isProdTFinished = false;

    public void start() {
        Thread prodT = new Thread(new Producer(transactionBlockingQueue));
        Thread cons1T = new Thread(new Consumer(transactionBlockingQueue));
        Thread cons2T = new Thread(new Consumer(transactionBlockingQueue));

        prodT.start();
        cons1T.start();
        cons2T.start();
        threadList.add(prodT);
        threadList.add(cons1T);
        threadList.add(cons2T);

        try {
            prodT.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (true) {
            if (!prodT.isAlive()) {
                isProdTFinished = true;
                try {
                    cons1T.join();
//                    System.out.println("111111111111111111111111111111111111111");
                    cons2T.join();
//                    System.out.println("222222222222222222222222222222222222222");
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
        System.out.println("Final balance: " + account.getFormattedBalance());
    }


    class Producer implements Runnable {
        List<Transaction> transactions = TransactionGenerator.readDataFile();
        private final ArrayBlockingQueue<Transaction> transactionBlockingQueue;

        public Producer(ArrayBlockingQueue<Transaction> queue) {
            this.transactionBlockingQueue = queue;
        }

        @Override
        public void run() {
            for (Transaction transaction : transactions) {
                try {
                    transactionBlockingQueue.put(transaction);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class Consumer implements Runnable {
        private final ArrayBlockingQueue<Transaction> transactionBlockingQueue;
        public Consumer(ArrayBlockingQueue<Transaction> queue) {
            this.transactionBlockingQueue = queue;
        }
        private int counter = 0;

        @Override
        public void run() {
            while (!isProdTFinished || !transactionBlockingQueue.isEmpty()) {

                try {
                    Transaction transaction = transactionBlockingQueue.poll(10, TimeUnit.SECONDS);
                    counter ++;

//                    lock.lock();
                    System.out.println(Thread.currentThread().getId() + "," + counter + "," + transaction.toString());
                    switch (transaction._type) {
                        case Deposit:
                            account.withOrDeposit(transaction._amountInCents, false);
//                            account.deposit(transaction._amountInCents);
                            break;
                        case Withdraw:
                            account.withOrDeposit(transaction._amountInCents, true);
//                            account.withdraw(transaction._amountInCents);
                            break;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
//                    lock.unlock();
                }
            }
        }
    }

    public static void main(String[] args) {

        ConcurrentBankingApp c = new ConcurrentBankingApp();
        c.start();
    }

}

/*
class Producer implements Runnable {
    List<Transaction> transactions = TransactionGenerator.readDataFile();
    private final ArrayBlockingQueue<Transaction> transactionBlockingQueue;

    public Producer(ArrayBlockingQueue<Transaction> queue) {
        this.transactionBlockingQueue = queue;
    }

    @Override
    public void run() {
        for (Transaction transaction : transactions) {
            try {
                transactionBlockingQueue.put(transaction);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer implements Runnable {
    private final ArrayBlockingQueue<Transaction> transactionBlockingQueue;
    BankAccount account = new BankAccount();

    public Consumer(ArrayBlockingQueue<Transaction> queue) {
        this.transactionBlockingQueue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Transaction transaction = transactionBlockingQueue.take();
//                System.out.println(transaction.toString());
                switch (transaction._type) {
                    case Withdraw:
                        account.deposit(transaction._amountInCents);
                        break;
                    case Deposit:
                        account.withdraw(transaction._amountInCents);
                        break;
                }
                System.out.println(transaction.toString());
                System.out.println("Final balance 2 : " + account.getFormattedBalance());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}

*/
