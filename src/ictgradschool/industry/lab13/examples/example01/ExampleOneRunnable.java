package ictgradschool.industry.lab13.examples.example01;

/**
 * A simple Runnable that will continually increment the counter until its thread is interrupted.
 */
public class ExampleOneRunnable implements Runnable {

    private long counter = 0;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            counter++;
            System.out.println(counter);
        }
    }

    public long getCounter() {
        return counter;
    }
}
