package ictgradschool.industry.lab13.ex01;

import ictgradschool.Keyboard;

/**
 * Created by mshe666 on 12/12/2017.
 */
public class ExerciseOne {
    public void start() {
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                int counter = 1;
                while (!Thread.currentThread().isInterrupted() && counter < 1000000) {
                    counter++;
                    System.
                            out.println(counter);
                }
            }
        };
        Thread myThread = new Thread(myRunnable);
        myThread.start();

        Keyboard.readInput();

        System.out.println("Interrupting thread and waiting for it to finish...");

        try {
            myThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ExerciseOne e = new ExerciseOne();
        e.start();
    }

}
